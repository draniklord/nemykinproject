﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DataAccessLayer
{
    public class DesignTimeDbContextConfiguration : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DbContext>()
                .UseNpgsql("Host=localhost;Port=5432;Database=ClassSchedule;Username=postgres;Password=ruslan572");

            return new DataContext(optionsBuilder.Options);
        }
    }
}
