﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    public class DirectionOfStudyConfiguration : IEntityTypeConfiguration<DirectionOfStudy>
    {
        public void Configure(EntityTypeBuilder<DirectionOfStudy> builder)
        {
            builder.HasKey(direction => direction.Id);

            builder.HasMany(group => group.Groups);

            builder.HasMany(discipline => discipline.Disciplines);

        //    builder
        //        .HasOne(departament => departament.GraduatingDepartment)
        //        .WithMany(direction => direction.DirectionsOfStudy);
        }
    }
}
