﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    public class GraduatingDepartmentConfiguration : IEntityTypeConfiguration<GraduatingDepartment>
    {
        public void Configure(EntityTypeBuilder<GraduatingDepartment> builder)
        {
            builder.HasKey(department => department.Id);

            //builder
            //    .HasMany(direction => direction.DirectionsOfStudy)
            //    .WithOne(department => department.GraduatingDepartment);
        }
    }
}
