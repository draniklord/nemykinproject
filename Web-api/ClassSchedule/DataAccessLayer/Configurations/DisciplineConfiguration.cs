﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    public class DisciplineConfiguration : IEntityTypeConfiguration<Discipline>
    {
        public void Configure(EntityTypeBuilder<Discipline> builder)
        {
            builder.HasKey(discipline => discipline.Id);

            builder
                .HasMany(discipline => discipline.Teachers)
                .WithOne(ticher => ticher.Discipline);
                //.HasForeignKey(ticher => ticher.TeacherId);

            builder
                .HasMany(discipline => discipline.Groups)
                .WithMany(group => group.Discipline);
        }
    }
}