﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    public partial class InitialCreate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Teachers_Disciplines_DisciplineId",
                table: "Teachers");

            migrationBuilder.AlterColumn<int>(
                name: "DisciplineId",
                table: "Teachers",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Teachers_Disciplines_DisciplineId",
                table: "Teachers",
                column: "DisciplineId",
                principalTable: "Disciplines",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Teachers_Disciplines_DisciplineId",
                table: "Teachers");

            migrationBuilder.AlterColumn<int>(
                name: "DisciplineId",
                table: "Teachers",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Teachers_Disciplines_DisciplineId",
                table: "Teachers",
                column: "DisciplineId",
                principalTable: "Disciplines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
