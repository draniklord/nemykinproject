﻿namespace DataAccessLayer.Models
{
    /// <summary>
    /// Преподаватель
    /// </summary>
    public class Teacher
    {
        /// <summary>
        /// Идентификатор преподавателя
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Имя преподавателя
        /// </summary>
        public string? Firstname { get; set; }

        /// <summary>
        /// Фамилия преподавателя
        /// </summary>
        public string? Lastname { get; set; }

        /// <summary>
        /// Дисциплина преподавателя
        /// </summary>
        public Discipline Discipline { get; set; }
        public int? DisciplineId { get; set; }

        public Role? Role { get; set; }
    }
}