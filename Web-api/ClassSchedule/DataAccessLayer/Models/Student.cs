﻿namespace DataAccessLayer.Models
{
    /// <summary>
    /// Студент
    /// </summary>
    public class Student
    {
        /// <summary>
        /// Индивидуальный номер студента
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Имя студента
        /// </summary>
        public string? FirstName { get; set; }

        /// <summary>
        /// Фамилия студента
        /// </summary>
        public string? LastName { get; set; }

        /// <summary>
        /// Группа, в которой учится студент
        /// </summary>
        public Group? Group { get; set; }
        public int? GroupId { get; set; }

        public Role? Role { get; set; }
    }
}
