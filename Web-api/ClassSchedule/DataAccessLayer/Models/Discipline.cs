﻿using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    /// <summary>
    /// Дисциплина
    /// </summary>
    public class Discipline
    {
        /// <summary>
        /// Номер дисциплины
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Название дисциплины
        /// </summary>
        /// 
        public string? Name { get; set; }

        /// <summary>
        /// Группы по данной дисциплине
        /// </summary>
        public ICollection<Group>? Groups { get; set; }

        /// <summary>
        /// Преподаватели данной дисциплины
        /// </summary>
        public ICollection<Teacher>? Teachers { get; set; }
    }
}
