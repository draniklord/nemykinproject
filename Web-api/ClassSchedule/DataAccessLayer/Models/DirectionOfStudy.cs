﻿namespace DataAccessLayer.Models
{
    /// <summary>
    /// Направление подготовки
    /// </summary>
    public class DirectionOfStudy
    {
        /// <summary>
        /// Номер направления подготовки
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Наименование направления
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Группы данного направления
        /// </summary>
        public ICollection<Group>? Groups { get; set; }

        /// <summary>
        /// Дисциплины данного направления
        /// </summary>
        public ICollection<Discipline>? Disciplines { get; set; }

        /// <summary>
        /// Выпускающая кафедра данного направления
        /// </summary>
        public GraduatingDepartment? GraduatingDepartment { get; set; }
        public int? GraduatingDepartmentId { get; set; }
    }
}
