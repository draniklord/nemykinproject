﻿namespace DataAccessLayer.Models
{
    /// <summary>
    /// Группа
    /// </summary>
    public class Group
    {
        /// <summary>
        /// Номер группы
        /// </summary>
        /// 
        public int? Id { get; set; }

        /// <summary>
        /// Название группы
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Студенты в данной группе
        /// </summary>
        public ICollection<Student> Student { get; set; }

        /// <summary>
        /// Дисциплины у данной группы
        /// </summary>
        public ICollection<Discipline> Discipline { get; set; }
    }
}