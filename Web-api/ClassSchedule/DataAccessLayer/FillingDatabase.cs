﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer
{
    public class FillingDatabase
    {
        public static void FillingData(DataContext dataContext)
        {
            var departament = new GraduatingDepartment 
            { 
                Id = 2,
                AudienceNumbers = 100,
            };
            dataContext.GraduatingDepartments.AddRange(departament);

            var directionOfStudy = new[]
            {
                new DirectionOfStudy{ Id = 4, Name = "Электропривод", GraduatingDepartmentId = 2 },
                new DirectionOfStudy{ Id = 5, Name = "Электроснабжение", GraduatingDepartmentId = 2 },
                new DirectionOfStudy{ Id = 6, Name = "Теплоснабжение", GraduatingDepartmentId = 2 },
            };
            dataContext.DirectionOfStudies.AddRange(directionOfStudy);         

            var group = new[]
            {
                new Group{Id = 4, Name = "ЭА-191", Discipline = new List<Discipline>(), Student = new List<Student>() },
                new Group{Id = 5, Name = "Э-201", Discipline = new List<Discipline>(), Student = new List<Student>() },
                new Group{Id = 6, Name = "ЭТ-191", Discipline = new List<Discipline>(), Student = new List<Student>() }
            };

            foreach (var item in directionOfStudy)
            {
            //    var find = dataContext.GraduatingDepartments.Find(1);
            //    item.GraduatingDepartment = find;
                departament.DirectionsOfStudy.Add(item);
            }

            dataContext.SaveChanges();


            //modelBuilder.Entity<GraduatingDepartment>().HasData(departament);
            //modelBuilder.Entity<DirectionOfStudy>().HasData(directionOfStudy);
            //modelBuilder.Entity<Group>().HasData(group);

            //modelBuilder.Entity<Group>().HasData(new Group
            //{
            //    GroupId = 4,
            //    GroupName = "ЭА-191",
            //    Discipline = new List<Discipline>(),
            //    Student = new List<Student>()
            //});

            //modelBuilder.Entity<Group>().HasData(new Group
            //{
            //    GroupId = 5,
            //    GroupName = "Э-201",
            //    Discipline = new List<Discipline>(),
            //    Student = new List<Student>()
            //});

            //modelBuilder.Entity<Group>().HasData(new Group
            //{
            //    GroupId = 6,
            //    GroupName = "ЭТ-191",
            //    Discipline = new List<Discipline>(),
            //    Student = new List<Student>()
            //});

            //modelBuilder.Entity<Discipline>().HasData(new Discipline
            //{
            //    DisciplineId = 4,
            //    DisciplineName = "Электроника",
            //    GroupId = 4,
            //    Groups = new List<Group>(),
            //    Teachers = new List<Teacher>()
            //});

            //modelBuilder.Entity<Discipline>().HasData(new Discipline
            //{
            //    DisciplineId = 5,
            //    DisciplineName = "Электрические машины",
            //    GroupId = 5,
            //    Groups = new List<Group>(),
            //    Teachers = new List<Teacher>()
            //});

            //modelBuilder.Entity<Discipline>().HasData(new Discipline
            //{
            //    DisciplineId = 6,
            //    DisciplineName = "Паровая энергетика",
            //    GroupId = 6,
            //    Groups = new List<Group>(),
            //    Teachers = new List<Teacher>()
            //});

            //modelBuilder.Entity<Teacher>().HasData(new Teacher
            //{
            //    TeacherId = 4,
            //    Firstname = "Александр",
            //    Lastname = "Белоусов",
            //    DisciplineId = 4,
            //    Role = Role.Teacher,
            //    Discipline = new Discipline()
            //});

            //modelBuilder.Entity<Teacher>().HasData(new Teacher
            //{
            //    TeacherId = 5,
            //    Firstname = "Владимир",
            //    Lastname = "Сотников",
            //    DisciplineId = 5,
            //    Role = Role.Teacher,
            //    Discipline = new Discipline()
            //});

            //modelBuilder.Entity<Teacher>().HasData(new Teacher
            //{
            //    TeacherId = 6,
            //    Firstname = "Наталья",
            //    Lastname = "Корнилова",
            //    DisciplineId = 6,
            //    Role = Role.Teacher,
            //    Discipline = new Discipline()
            //});

            //modelBuilder.Entity<Student>().HasData(new Student
            //{
            //    StudentId = 4,
            //    FirstName = "Даниил",
            //    LastName = "Серенко",
            //    GroupId=4,
            //    Role=Role.Student,
            //    Group = new Group()
            //});

            //modelBuilder.Entity<Student>().HasData(new Student
            //{
            //    StudentId = 5,
            //    FirstName = "Игорь",
            //    LastName = "Чуев",
            //    GroupId = 5,
            //    Role = Role.Student,
            //    Group = new Group()
            //});

            //modelBuilder.Entity<Student>().HasData(new Student
            //{
            //    StudentId = 6,
            //    FirstName = "Андрей",
            //    LastName = "Дъяков",
            //    GroupId = 6,
            //    Role = Role.Student,
            //    Group = new Group()
            //});
        }
    }
}
