﻿using DataAccessLayer.Configurations;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer
{
    public sealed class DataContext : DbContext
    {        
        public DbSet<GraduatingDepartment>? GraduatingDepartments { get; set; }
        public DbSet<DirectionOfStudy>? DirectionOfStudies { get; set; }
        public DbSet<Discipline>? Disciplines { get; set; }
        public DbSet<Group>? Groups { get; set; }
        public DbSet<Student>? Students { get; set; }
        public DbSet<Teacher>? Teachers { get; set; }

        public DataContext(DbContextOptions options) : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
            Database.Migrate();
        }

        public string dbConnection;

        public DataContext(string dbConnection)
        {
            this.dbConnection = dbConnection;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (dbConnection != null)
            {
                optionsBuilder.UseNpgsql(dbConnection);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder
                .Entity<Student>()
                .Property(x => x.Role)
                .HasConversion(
                    v => v.ToString(),
                    v => (Role)Enum.Parse(typeof(Role), v));

            modelBuilder
                .Entity<Teacher>()
                .Property(x => x.Role)
                .HasConversion(
                     v => v.ToString(),
                     v => (Role)Enum.Parse(typeof(Role), v));

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DirectionOfStudyConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DisciplineConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(GraduatingDepartmentConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(GroupConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(StudentConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(TeacherConfiguration).Assembly);
        }
    }
}
