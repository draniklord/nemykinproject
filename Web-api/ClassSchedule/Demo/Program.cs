﻿using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

var optionsBuilder = new DbContextOptionsBuilder<DataContext>()
                .UseNpgsql("Host=localhost;Port=5432;Database=testProj;Username=postgres;Password=ruslan572");

var context = new DataContext(optionsBuilder.Options);

context.Database.Migrate();

//context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

context.GraduatingDepartments.Add(new GraduatingDepartment()
{
    Id = 2,
    AudienceNumbers = 123,
    DirectionsOfStudy = new List<DirectionOfStudy>(),
    //DirectionOfStudyId = 2,
});

context.Groups.Add(new Group()
{
    Id = 2,
    Discipline = new List<Discipline>(),
    Name = "ЭА-191",
    Student = new List<Student>(),
    //DisciplineId = 2,
    //StudentId = 2,
});

context.DirectionOfStudies.Add(new DirectionOfStudy()
{
    Id = 2,
    Name = "Tay",
    Disciplines = new List<Discipline>(),
    GraduatingDepartment = new GraduatingDepartment(),
    Groups = new List<Group>(),
    //DisciplineId = 2,
    GraduatingDepartmentId = 2,
    //GroupId = 2,
});

context.Disciplines.Add(new Discipline()
{
    Id = 2,
    Name = "Electr",
    Teachers = new List<Teacher>(),
    Groups = new List<Group>(),
    //GroupsId = 2,
    //TeachersId = 2,
});

context.Teachers.Add(new Teacher()
{
    Id = 2,
    Firstname = "Вайб",
    Lastname = "Универов",
    Discipline = new Discipline(),
    Role = Role.Teacher,
    DisciplineId= 2,
});


context.Students.Add(new Student()
{
    Id = 2, 
    FirstName = "Олег",
    LastName = "Винников",
    Group = new Group(),
    Role = Role.Student,
    GroupId = 2,
});

context.SaveChanges();
context.Dispose();

Console.ReadKey();