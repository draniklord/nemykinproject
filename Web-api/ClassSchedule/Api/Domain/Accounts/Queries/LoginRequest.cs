﻿using Api.Services;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Accounts.Queries
{
    public class LoginRequest : IRequest<string>
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public LoginRequest(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }

    public class LoginHandler : IRequestHandler<LoginRequest, string>
    {
        private readonly DataContext _dataContext;
        private readonly IJwtGenerator _jwtGenerator;

        public LoginHandler(DataContext dataContext, IJwtGenerator jwtGenerator)
        {
            _dataContext = dataContext;
            _jwtGenerator = jwtGenerator;
        }

        public async Task<string> Handle(LoginRequest request, CancellationToken cancellationToken)
        {
            var student = await _dataContext.Students.FirstAsync(s => s.FirstName == request.Login && s.LastName == request.Password);
            var token = _jwtGenerator.Create(student.FirstName, student.Role.ToString());
            return token;
        }
    }
}
