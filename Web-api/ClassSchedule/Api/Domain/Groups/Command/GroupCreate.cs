﻿using DataAccessLayer;
using MediatR;

namespace Api.Domain.Groups.Command
{
    public class GroupCreate : IRequest
    {
        /// <summary>
        /// Номер группы
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название группы
        /// </summary>
        public string? Name { get; set; }

        public GroupCreate(int groupId, string? groupName)
        {
            Id = groupId;
            Name = groupName;
        }
    }

    public class GroupCreateHandler : IRequestHandler<GroupCreate>
    {
        private DataContext _dataContext;

        public GroupCreateHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(GroupCreate request, CancellationToken cancellationToken)
        {
            await _dataContext.Groups.AddAsync(new DataAccessLayer.Models.Group()
            {
                Id = request.Id,
                Name = request.Name
            }, cancellationToken);

            await _dataContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
