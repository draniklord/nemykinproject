﻿using AutoMapper;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Groups.Queries
{
    public class GetGroupQuery : IRequest<IEnumerable<GroupDto>>
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }

    public class GetGroupQueryValidation : AbstractValidator<GetGroupQuery>
    {
        public GetGroupQueryValidation()
        {
            RuleFor(query => query.Skip).GreaterThanOrEqualTo(0).LessThanOrEqualTo(100);
            RuleFor(query => query.Take).GreaterThanOrEqualTo(1).LessThanOrEqualTo(100);
        }
    }

    public class GetGroupQueryHandler : IRequestHandler<GetGroupQuery, IEnumerable<GroupDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public GetGroupQueryHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<GroupDto>> Handle(GetGroupQuery request, CancellationToken cancellationToken)
        {
            var groupsData = await _dataContext.Groups
                .Skip(request.Skip)
                .Take(request.Take)
                .Include(group => group.Discipline)
                .Include(group => group.Student)
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<GroupDto>>(groupsData);
        }
    }
}
