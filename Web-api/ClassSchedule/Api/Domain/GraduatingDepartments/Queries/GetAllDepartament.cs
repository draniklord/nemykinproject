﻿using AutoMapper;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.GraduatingDepartments.Queries
{
    public class GetAllDepartament : IRequest<IEnumerable<GraduatingDepartmentDto>>
    {
        public int Take { get; set; }
    }

    public class GetAllDepartamentValidation : AbstractValidator<GetAllDepartament>
    {
        public GetAllDepartamentValidation()
        {
            RuleFor(query => query.Take).GreaterThanOrEqualTo(1).LessThanOrEqualTo(100);
        }
    }

    public class GetGroupQueryHandler : IRequestHandler<GetAllDepartament, IEnumerable<GraduatingDepartmentDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public GetGroupQueryHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<GraduatingDepartmentDto>> Handle(GetAllDepartament request, CancellationToken cancellationToken)
        {
            var departamenttList = await _dataContext.GraduatingDepartments
                                .Take(request.Take)
                                .Include(direction => direction.DirectionsOfStudy)
                                    .ThenInclude(group => group.Groups)
                                        .ThenInclude(student => student.Student)
                                .Include(direction => direction.DirectionsOfStudy)
                                    .ThenInclude(discipline => discipline.Disciplines)
                                        .ThenInclude(teacher => teacher.Teachers)
                                .AsNoTracking()
                                .ToListAsync(cancellationToken);

            if (departamenttList == null)
            {
                return null;
            }
            return _mapper.Map<IEnumerable<GraduatingDepartmentDto>>(departamenttList);
        }
    }
}
