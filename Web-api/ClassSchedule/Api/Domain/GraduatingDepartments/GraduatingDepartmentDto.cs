﻿using Api.Domain.DirectionsOfStudy;

namespace Api.Domain.GraduatingDepartments
{
    /// <summary>
    /// Выпускающая кафедра
    /// </summary>
    public class GraduatingDepartmentDto
    {
        /// <summary>
        /// Номер кафедры, он же этаж
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Направления подготовки
        /// </summary>
        public List<DirectionOfStudyDto> DirectionsOfStudy { get; set; }

        /// <summary>
        /// Номера аудиториий
        /// </summary>
        public int AudienceNumbers { get; set; }
    }
}