﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Domain.GraduatingDepartments
{
    public class DepartamentProfile : Profile
    {
        public DepartamentProfile()
        {
            CreateMap<GraduatingDepartmentDto, GraduatingDepartment>();
            CreateMap<GraduatingDepartment, GraduatingDepartmentDto>();
        }
    }
}
