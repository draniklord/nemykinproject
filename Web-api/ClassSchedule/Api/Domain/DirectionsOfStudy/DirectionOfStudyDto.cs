﻿using Api.Domain.Disciplines;
using Api.Domain.GraduatingDepartments;
using Api.Domain.Groups;

namespace Api.Domain.DirectionsOfStudy
{
    /// <summary>
    /// Направление подготовки
    /// </summary>
    public class DirectionOfStudyDto
    {
        /// <summary>
        /// Номер направления подготовки
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование направления
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Группы данного направления
        /// </summary>
        public List<GroupDto> Groups { get; set; }

        /// <summary>
        /// Дисциплины данного направления
        /// </summary>
        public List<DisciplineDto> Disciplines { get; set; }

        /// <summary>
        /// Выпускающая кафедра данного направления
        /// </summary>
        public GraduatingDepartmentDto GraduatingDepartment { get; set; }
    }
}
