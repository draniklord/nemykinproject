﻿using DataAccessLayer;
using DataAccessLayer.Models;
using MediatR;

namespace Api.Domain.Disciplines.Command
{
    public class CreateDisciplines : IRequest
    {
        /// <summary>
        /// Номер дисциплины
        /// </summary>
        public int DisciplineId { get; set; }

        /// <summary>
        /// Название дисциплины
        /// </summary>
        public string? DisciplineName { get; set; }

        public CreateDisciplines(int disciplineId, string? disciplineName)
        {
            DisciplineId = disciplineId;
            DisciplineName = disciplineName;
        }     
    }

    public class GroupCreateHandler : IRequestHandler<CreateDisciplines>
    {
        private DataContext _dataContext;

        public GroupCreateHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(CreateDisciplines request, CancellationToken cancellationToken)
        {
            await _dataContext.Disciplines.AddAsync(new Discipline()
            {
                Id = request.DisciplineId,
                Name = request.DisciplineName
            }, cancellationToken);

            await _dataContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
