﻿using DataAccessLayer;
using MediatR;

namespace Api.Domain.Disciplines.Command
{
    public class UpdateDiscipline : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }    

        public class UpdateProductCommandHandler : IRequestHandler<UpdateDiscipline, int>
        {
            private DataContext _dataContext;

            public UpdateProductCommandHandler(DataContext dataContext)
            {
                _dataContext = dataContext;
            }

            public async Task<int> Handle(UpdateDiscipline command, CancellationToken cancellationToken)
            {
                var discipline = _dataContext.Disciplines.Where(a => a.Id == command.Id).FirstOrDefault();

                discipline.Name = command.Name;

                _dataContext.SaveChanges();

                return (int)discipline.Id;
            }
        }
    }
}
