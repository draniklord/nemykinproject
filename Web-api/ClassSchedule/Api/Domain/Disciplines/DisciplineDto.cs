﻿using Api.Domain.Groups;
using Api.Domain.Teachers;

namespace Api.Domain.Disciplines
{
    /// <summary>
    /// Дисциплина
    /// </summary>
    public class DisciplineDto
    {
        /// <summary>
        /// Номер дисциплины
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название дисциплины
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Группы по данной дисциплине
        /// </summary>
        public List<GroupDto> Groups { get; set; }

        /// <summary>
        /// Преподаватели данной дисциплины
        /// </summary>
        public List<TeacherDto> Teachers { get; set; }
    }
}
