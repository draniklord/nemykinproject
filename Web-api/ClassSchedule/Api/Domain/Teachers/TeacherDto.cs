﻿using Api.Domain.Disciplines;

namespace Api.Domain.Teachers
{
    /// <summary>
    /// Преподаватель
    /// </summary>
    public class TeacherDto
    {
        /// <summary>
        /// Идентификатор преподавателя
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя преподавателя
        /// </summary>
        public string Firstname { get; set; }

        /// <summary>
        /// Фамилия преподавателя
        /// </summary>
        public string Lastname { get; set; }

        /// <summary>
        /// Дисциплина преподавателя
        /// </summary>
        public DisciplineDto Discipline { get; set; }

        public Role Role { get; set; }
    }
}