﻿using Api.Domain.Disciplines;
using Api.Domain.Disciplines.Command;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// Дисциплины
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DisciplineController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator
        {
            get
            {
                return _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
            }
        }

        public DisciplineController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Добавить новую дисциплину
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CreateDiscipline(int id, string name)
        {
            if (ModelState.IsValid)
            {
                var createDiscipline = new CreateDisciplines( id,  name);
                await _mediator.Send(createDiscipline);
                return Ok();
            }

            var error = new ApiError
            {
                Code = 400,
                Message = "Дисциплина с таким ID уже существует"
            };
            return BadRequest(error);
        }

        /// <summary>
        /// Изменить дисциплину
        /// </summary>
        /// <param name="id"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateDiscipline(int id, UpdateDiscipline command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Удалить дисциплину
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteDiscipline { Id = id }));
        }
    }
}
