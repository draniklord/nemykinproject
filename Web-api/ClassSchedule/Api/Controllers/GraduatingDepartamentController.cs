﻿using Api.Domain.Disciplines.Command;
using Api.Domain.GraduatingDepartments;
using Api.Domain.GraduatingDepartments.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GraduatingDepartamentController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        public GraduatingDepartamentController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Получение информации о кафедрах
        /// </summary>
        /// <param name="getAllDepartament"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GraduatingDepartmentDto>>> GetAllDepartament([FromQuery] GetAllDepartament getAllDepartament)
        {
            return Ok(await Mediator.Send(new GetAll()));
        }
    }
}
