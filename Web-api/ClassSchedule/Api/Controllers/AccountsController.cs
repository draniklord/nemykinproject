﻿using Api.Domain.Accounts.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AccountsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Получить токен авторизации
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("/login")]
        public async Task<ActionResult<string>> Login(string login, string password)
        {
            try
            {
                var jwt = await _mediator.Send(new LoginRequest(login, password));

                return Ok(jwt);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Проверка Роли
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Student")]
        [HttpGet("/me")]
        public ActionResult<string> Login()
        {
            return Ok(HttpContext.User.Claims?.FirstOrDefault(x => x.Type == ClaimsIdentity.DefaultNameClaimType)?.Value);
        }
    }
}
